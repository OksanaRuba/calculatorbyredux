import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Calculator from './components/calculator';


ReactDOM.render(//Это сделает наш экземпляр хранилища доступным для всех компонентов, которые располагаются в Provider компоненте.
  <Provider store={store}> 
    <Calculator />
  </Provider>, document.getElementById('root')
);




