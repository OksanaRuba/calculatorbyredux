import { ADD_OPERATION, CLEAR, EQUAL } from '../redux/types';

// Дія (action) - це JavaScript-об'єкт, який лаконічно описує суть зміни. Обов'язково це наявність властивості type, значенням якого зазвичай є строка.
export function add_operation(payload){
    return { 
        type: ADD_OPERATION,
        payload 
    }
}

export function clear(){
    return {
        type: CLEAR
    }
}

export function equal(payload){
    return {
        type: EQUAL,
        payload 
    }
}
