import { ADD_OPERATION, CLEAR, EQUAL } from '../redux/types';

const defaultValue = {
    value: 0
} 
//"Actions описывает факт, что что-то произошло, но не указывает, как состояние приложения должно измениться в ответ, это работа для Reducerа"(офф. документация)

 export const calcReducer = (state = defaultValue, action) => {
    switch(action.type){
        case 'ADD_OPERATION':
            return{
                ...state,
                value: state.value == 0 ? action.payload : state.value + action.payload
            }
        case 'CLEAR':
            return{
                ...state,
                value: 0
            }
        case 'EQUAL':
            console.log('action', action);
            return{
                ...state,
                value: eval(action.payload) //Функція eval() є небезпечною в плані безпеки. Рядок коду може бути злоякісним якщо отримано від третіх осіб, тобто зловмисники можуть передати Вам код який може змінити вміст сторінки, перенаправити на іншу сорінку, вкрасти дані тощо.
            }
        default:
            return state;
    }
};
