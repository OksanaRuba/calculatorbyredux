import { createStore } from 'redux';
import { calcReducer } from '../redux/rootReducer';

export default createStore(
  calcReducer
);

//Store хранит состояние приложения. Единственный путь изменить store - это отправить действие (dispatch action).